<?php

namespace Drupal\conditional_message\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\Entity\Role;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Conditional message entity.
 *
 * @ingroup conditional_message
 *
 * @ContentEntityType(
 *   id = "conditional_message",
 *   label = @Translation("Conditional message"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\conditional_message\ConditionalMessageListBuilder",
 *     "views_data" =
 *   "Drupal\conditional_message\Entity\ConditionalMessageViewsData",
 *     "translation" =
 *   "Drupal\conditional_message\ConditionalMessageTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\conditional_message\Form\ConditionalMessageForm",
 *       "add" = "Drupal\conditional_message\Form\ConditionalMessageForm",
 *       "edit" = "Drupal\conditional_message\Form\ConditionalMessageForm",
 *       "delete" =
 *   "Drupal\conditional_message\Form\ConditionalMessageDeleteForm",
 *     },
 *     "access" =
 *   "Drupal\conditional_message\ConditionalMessageAccessControlHandler",
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\conditional_message\ConditionalMessageHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "conditional_message",
 *   data_table = "conditional_message_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer conditional message entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/content/conditional-message/{conditional_message}",
 *     "add-form" = "/admin/content/conditional-message/add",
 *     "edit-form" =
 *   "/admin/content/conditional-message/{conditional_message}/edit",
 *     "delete-form" =
 *   "/admin/content/conditional-message/{conditional_message}/delete",
 *     "collection" = "/admin/content/conditional-message",
 *   },
 *   field_ui_base_route = "conditional_message.settings"
 * )
 */
class ConditionalMessage extends ContentEntityBase implements ConditionalMessageInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * Get condition method.
   */
  public function getConditions() {
    $conditions = [];
    $conditions_values = $this->get('conditions')->getValue();
    foreach ($conditions_values as $raw_value) {
      array_push($conditions, $raw_value['value']);
    }

    return $conditions;
  }

  /**
   * Get role options.
   */
  public function getRoleOptions() {
    $role_options = [];
    $role_values = $this->get('role_options')->getValue();
    foreach ($role_values as $raw_value) {
      array_push($role_options, $raw_value['value']);
    }

    return $role_options;
  }

  /**
   * Get path options.
   *
   * @return array
   *   An array of options regarding the path condition.
   */
  public function getPathOptions() {
    $path_options = [];
    $path_values = $this->get('path_options')->getValue();
    foreach ($path_values as $raw_value) {
      if ($languages_prefixes = \Drupal::config('language.negotiation')->get('url.prefixes')) {
        foreach ($languages_prefixes as $pref) {
          $pref = ($pref) ? '/' . $pref : '/';
          $value_lp = ($raw_value['value'] != '/') ? $pref . $raw_value['value'] : $pref;
          $value_lp = str_replace('//', '/', $value_lp);
          array_push($path_options, $value_lp);
        }
      }
      else {
        array_push($path_options, $raw_value['value']);
      }
    }

    return $path_options;
  }

  /**
   * Get Content Type options.
   *
   * @return array
   *   An array of options regarding the content type condition.
   */
  public function getContentTypeOptions() {
    $content_type_options = [];
    $content_type_values = $this->get('content_type_options')->getValue();
    foreach ($content_type_values as $raw_value) {
      array_push($content_type_options, $raw_value['value']);
    }

    return $content_type_options;
  }

  /**
   * Get message text.
   */
  public function getMessage() {
    return $this->get('message')->getValue()[0]['value'];
  }

  /**
   * Get background color.
   */
  public function getBackGroundColor() {
    return $this->get('bg_color')->getValue()[0]['value'];
  }

  /**
   * Get font color.
   */
  public function getFontColor() {
    return $this->get('font_color')->getValue()[0]['value'];
  }

  /**
   * Get position.
   */
  public function getPosition() {
    return $this->get('position')->getValue()[0]['value'];
  }

  /**
   * Get target.
   */
  public function getTarget() {
    return $this->get('target')->getValue()[0]['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDescription(t('Uncheck to disable the message without deleting it.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Descriptive name'))
      ->setDescription(t('Administrative name or short description to identify this message internally.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    // TODO make the conditions config be required if condition selected.
    $fields['conditions'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Conditions'))
      ->setDescription(t('Select the conditions in which the message will be displayed. Several conditions can be selected.'))
      ->setSettings([
        'allowed_values' => [
          'session' => 'once per session',
          'close' => 'until manually closed',
          'role' => 'to certain user roles',
          'path' => 'on certain paths',
          'content_type' => 'on certain content types',
        ],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE);

    /* Conditions optional fields */

    // Get available roles in an array.
    $all_roles = Role::loadMultiple();
    $roles = array_combine(array_keys($all_roles), array_map('ucfirst', array_keys($all_roles)));

    $fields['role_options'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('User roles'))
      ->setDescription(t('Select roles that will see the message'))
      ->setSettings([
        'allowed_values' => $roles,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE);

    // Get available content types.
    $all_types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    foreach ($all_types as $machine_name => $content_type) {
      $content_types[$machine_name] = $content_type->get('name');
    }

    // TODO path should be an auto complete field with path validation.
    $fields['path_options'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Paths'))
      ->setDescription(t('Enter one path per line starting with a slash. Aliases are considered equivalent so /node/1 and /first-page could both work.'))
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE);

    $fields['content_type_options'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Content types'))
      ->setDescription(t('Select content types that will trigger the message'))
      ->setSettings([
        'allowed_values' => $content_types,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(FALSE);

    /* End of conditions optional fields */
    $fields['message'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Message'))
      ->setDescription(t('Set the message that will be displayed to users when the conditions are met.'))
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setDefaultValue('Enter a custom message at /admin/content/conditional-message')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['bg_color'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Background color'))
      ->setDescription(t('Text color in HEX or color name (e.g.119944 or green).'))
      ->setSettings([
        'max_length' => 7,
        'text_processing' => 0,
      ])
      ->setDefaultValue('119944')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    // Standard field, used as unique if primary index.
    $fields['font_color'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Font color'))
      ->setDescription(t('Text color in HEX or color name (e.g.EEEEEE or white).'))
      ->setSettings([
        'length' => 7,
        'max_length' => 7,
        'text_processing' => 0,
      ])
      ->setDefaultValue('EEEEEE')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['position'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Message position'))
      ->setDescription(t('Select the desired position for the conditional message.'))
      ->setSettings([
        'allowed_values' => [
          'top' => 'Top',
          'bottom' => 'Bottom',
        ],
      ])
      ->setDefaultValue('top')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['target'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target'))
      ->setDescription(t('Enter an id, class or tag (any CSS selector) of the container where the message should be appended. If blank, "body" will be used.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('body')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Conditional message entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
