<?php

namespace Drupal\conditional_message\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Conditional message entities.
 *
 * @ingroup conditional_message
 */
interface ConditionalMessageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Conditional message name.
   *
   * @return string
   *   Name of the Conditional message.
   */
  public function getName();

  /**
   * Sets the Conditional message name.
   *
   * @param string $name
   *   The Conditional message name.
   *
   * @return \Drupal\conditional_message\Entity\ConditionalMessageInterface
   *   The called Conditional message entity.
   */
  public function setName($name);

  /**
   * Gets the Conditional message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Conditional message.
   */
  public function getCreatedTime();

  /**
   * Sets the Conditional message creation timestamp.
   *
   * @param int $timestamp
   *   The Conditional message creation timestamp.
   *
   * @return \Drupal\conditional_message\Entity\ConditionalMessageInterface
   *   The called Conditional message entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Conditional message published status indicator.
   *
   * Unpublished Conditional message are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Conditional message is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Conditional message.
   *
   * @param bool $published
   *   TRUE sets the message to published, FALSE sets it to unpublished.
   *
   * @return \Drupal\conditional_message\Entity\ConditionalMessageInterface
   *   The called Conditional message entity.
   */
  public function setPublished($published);

}
