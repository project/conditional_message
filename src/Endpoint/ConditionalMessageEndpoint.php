<?php

namespace Drupal\conditional_message\Endpoint;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Defines the endpoint where the data is exposed.
 */
class ConditionalMessageEndpoint {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  private $currentUser;

  /**
   * ConditionalMessageEndpoint constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * Gathers and treats the data to be exposed.
   *
   * @return array
   *   A list of messages.
   */
  public function getEndpointData() {
    // Prepare the result data.
    $data = [];

    // Get all published conditional messages.
    $cm_storage = $this->entityTypeManager->getStorage('conditional_message');
    $cmids = $cm_storage->getQuery()
      ->condition('status', 1)
      ->execute();
    $messages = $cm_storage->loadMultiple($cmids);

    // Foreach message, check the conditions.
    // TODO review the logic of all conditions and combinations.
    // In the backend (here) we check for the role conditions.
    // In the front end (JS) we check for session, path and content type.
    foreach ($messages as $cmid => $message) {
      // Set each field value accordingly.
      $conditions = $message->getConditions();

      // Check for conditions and expose in the endpoint.
      $data[$cmid]['role'] = TRUE;
      foreach ($conditions as $condition) {
        switch ($condition) {
          case 'close':
            $data[$cmid]['close'] = TRUE;
            break;

          case 'role':
            // For now 'role' is the only backend check.
            $data[$cmid]['role'] = FALSE;
            $user = $this->currentUser;
            $selected_roles = $message->getRoleOptions();
            $user_roles = $user->getRoles();
            foreach ($selected_roles as $selected_role) {
              foreach ($user_roles as $role_value) {
                if ($selected_role === $role_value) {
                  $data[$cmid]['role'] = TRUE;
                  continue 2;
                }
              }
            }
            break;

          case 'path':
            $raw_selected_paths = $message->getPathOptions();
            $data[$cmid]['paths'] = array_map('trim', $raw_selected_paths);
            break;

          case 'content_type':
            $selected_types = $message->getContentTypeOptions();
            $data[$cmid]['types'] = array_filter($selected_types);
            break;
        }
      }

      // Determine if the message should be displayed or not.
      if ($data[$cmid]['role']) {
        $data[$cmid]['display'] = TRUE;
      }
    }

    return $data;
  }

}
