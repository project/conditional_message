<?php

namespace Drupal\conditional_message;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for conditional_message.
 */
class ConditionalMessageTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
