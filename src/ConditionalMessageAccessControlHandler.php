<?php

namespace Drupal\conditional_message;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Conditional message entity.
 *
 * @see \Drupal\conditional_message\Entity\ConditionalMessage.
 */
class ConditionalMessageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\conditional_message\Entity\ConditionalMessageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished conditional message entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published conditional message entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit conditional message entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete conditional message entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add conditional message entities');
  }

}
