<?php

namespace Drupal\conditional_message\Controller;

use Drupal\conditional_message\Endpoint\ConditionalMessageEndpoint;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Creates an endpoint for JS to check for uncached values from backend.
 *
 * @package Drupal\conditional_message\Controller
 */
class ConditionalMessageController extends ControllerBase {

  /**
   * The data collected from the endpoint.
   *
   * @var \Drupal\conditional_message\Endpoint\ConditionalMessageEndpoint
   */
  private $endpointData;

  /**
   * Construct method.
   */
  public function __construct(ConditionalMessageEndpoint $conditionalMessageEndpoint) {
    $this->endpointData = $conditionalMessageEndpoint;
  }

  /**
   * Outputs data in JSON format.
   */
  public function jsonOutput() {
    $data = $this->endpointData->getEndpointData();

    return new JsonResponse($data);
  }

  /**
   * Creates the endpoint.
   */
  public static function create(ContainerInterface $container) {
    $conditionalMessageEndpoint = $container->get('conditional_message.endpoint');

    return new static($conditionalMessageEndpoint);
  }

}
