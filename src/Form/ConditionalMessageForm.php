<?php

namespace Drupal\conditional_message\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Form controller for Conditional message edit forms.
 *
 * @ingroup conditional_message
 */
class ConditionalMessageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\conditional_message\Entity\ConditionalMessage $entity */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Invalidate attachments cache after saving. See hook_page_attachments().
    Cache::invalidateTags(['conditional_message_attachments']);

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Conditional message.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Conditional message.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.conditional_message.canonical', ['conditional_message' => $entity->id()]);
  }

}
