<?php

namespace Drupal\conditional_message\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Conditional message entities.
 *
 * @ingroup conditional_message
 */
class ConditionalMessageDeleteForm extends ContentEntityDeleteForm {


}
