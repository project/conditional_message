<?php

/**
 * @file
 * Contains conditional_message.page.inc.
 *
 * Page callback for Conditional message entities.
 * TODO review if this is needed, also delete template if not.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Conditional message templates.
 *
 * Default template: conditional_message.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_conditional_message(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
