/**
 * @file
 */

(function (Drupal, drupalSettings) {

  "use strict";

  Drupal.behaviors.conditional_message = {
    attach: function (context, settings) {
      // Run only once (not on ajax calls).
      if (context != document || drupalSettings.conditional_message === undefined) {
        return;
      }
      // Shorthand for the configurations.
      let config = drupalSettings.conditional_message;
      let htmls = [];
      for (let key in config) {
        // Bail if user already closed the message before.
        let cmuc = 'conditionalMessageUserClosed' + key;
        if (localStorage.getItem(cmuc) == config[key].hash) {
          continue;
        }
        (function (config, key, htmls) {
          this.queryEndpoint(config, key, htmls);
        }).call(this, config, key, htmls);
      }
    },
    queryEndpoint: function (config, key, htmls) {
      const request = new XMLHttpRequest();
      request.open('GET', Drupal.url("conditional_message_data_output"), true);
      request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
      request.onload = function () {
        if (this.status>= 200 && this.status < 400) {
          // Do not proceed if key not defined. Node deleted?
          const result = JSON.parse(this.responseText);
          if (result[key] == undefined) {
            return;
          }
          // Checks to see if the message should be displayed.  Config = backend logic, Result = frontend logic.
          let check = [];
          // Check and set sessions with localStorage.
          check['session'] = true;
          if (config[key].conditions.includes('session')) {
            let cmrs = 'conditionalMessageReadStatus' + key;
            let readStatus = localStorage.getItem(cmrs);
            if (readStatus !== config[key].hash) {
              check['session'] = true;
              localStorage.setItem(cmrs, config[key].hash);
            } else {
              check['session'] = false;
            }
          }
          // Check close button with localStorage.
          check['close'] = true;
          if (config[key].conditions.includes('close') && result[key].close) {
            let cmuc = 'conditionalMessageUserClosed' + key;
            let closeHash = localStorage.getItem(cmuc);
            if (closeHash == config[key].hash) {
              check['close'] = false;
            }
          }
          // Paths conditions.
          check['path'] = true;
          if (config[key].conditions.includes('path')
            && result[key].paths.indexOf(window.location.pathname.substr(config[key].base_path.length - 1)) < 0) {

            check['path'] = false;
          }
          // Content type conditions.
          check['type'] = true;
          if (config[key].conditions.includes('content_type') && result[key].hasOwnProperty('types')) {
            check['type'] = false;
            result[key].types.forEach(function (type) {
              let typeClass = 'page-node-type-' + type;
              if (document.querySelector('body').classList.contains(typeClass)) {
                check['type'] = true;
              }
            });
          }
          // Show message if all checks pass.
          if (config[key].status
            && result[key].display
            && check['session']
            && check['path']
            && check['type']
            && check['close']) {

            // Prepare color, either color name or HEX. If HEX color, add hash.
            if (/^([0-9A-F]{3}){1,2}$/i.test(config[key].bg_color)) {
              config[key].bg_color = '#' + config[key].bg_color;
            }
            if (/^([0-9A-F]{3}){1,2}$/i.test(config[key].color)) {
              config[key].color = '#' + config[key].color;
            }

            // Build the message div element.
            htmls[key] = document.createElement('div');
            htmls[key].setAttribute('class', 'conditional-message');
            htmls[key].setAttribute('data-cm-key', key);
            htmls[key].setAttribute('style', 'background-color:' + config[key].bg_color + '; color:' + config[key].color);
            if (result[key].close) {
              config[key].message += '<span>&times;</span>';
            }
            htmls[key].innerHTML = config[key].message;

            // Place the message in the page top or bottom.
            switch (config[key].position) {
              case  'bottom':
                htmls[key].classList.add('conditional-message-bottom');
                document.querySelector(config[key].target).append(htmls[key]);
                break;

              default:
                htmls[key].classList.add('conditional-message-top');
                document.querySelector(config[key].target).prepend(htmls[key]);
            }
            // Close button.
            document.querySelector('.conditional-message span').addEventListener('click', function () {
              let cmKey = this.parentElement.getAttribute('data-cm-key');
              this.parentElement.remove();
              let cmuc = 'conditionalMessageUserClosed' + cmKey;
              if (result[key].close) {
                localStorage.setItem(cmuc, config[key].hash);
              }
            });
          }
        }
      };
      request.onerror = function () {
        console.log('Conditional Message might not be working as expected: connection error.');
      }
      request.send();
    },
  };
})(Drupal, drupalSettings);
