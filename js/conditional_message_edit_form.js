/**
 * @file
 */

(function (Drupal) {

    "use strict";

    Drupal.behaviors.conditional_message_edit_form = {
      attach: function (context, settings) {
        // Show or hide options according to selected conditions.
        function toggleVisibility() {
          if (document.querySelector('#edit-conditions-role').checked) {
            document.querySelector('#edit-role-options-wrapper').style.display = 'block';
          }
          else {
            document.querySelector('#edit-role-options-wrapper').style.display = 'none';
          }
          if (document.querySelector('#edit-conditions-path').checked) {
            document.querySelector('#edit-path-options-wrapper').style.display = 'block';
          }
          else {
            document.querySelector('#edit-path-options-wrapper').style.display = 'none';
          }
          if (document.querySelector('#edit-conditions-content-type').checked) {
            document.querySelector('#edit-content-type-options-wrapper').style.display = 'block';
          }
          else {
            document.querySelector('#edit-content-type-options-wrapper').style.display = 'none';
          }
        }
        toggleVisibility();

        document.addEventListener('click', toggleVisibility);
      }
    }
})(Drupal);
